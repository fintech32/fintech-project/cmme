.PHONY: deploy install

deploy:
	ssh server 'cd /var/www/cmme.ga && git pull origin master && make install'

install: vendor/autoload.php .env

.env:
	cp .env.example .env init
